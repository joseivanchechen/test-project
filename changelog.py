import os
import sys
import subprocess as sp

'''
1. Get each commit message since last tag
$ git rev-list $PREV_HASH..HEAD
2. Parse the commit messages and get the Jira tasks (EF-*)
3. Get Jira summary on each task and add it to a list
4. Send list and version number to a Slack channel

If EF is "EF-000", we put the commit message in there instead of Jira summary
If a tag is pushed to prod/test, we send a message "Version X deployed".

Example commit message:
* [EF-000] This is a commit message
* [FIX][EF-150] This is a commit message
* [FIX][EF-200] Commit for a Jira task EF-200
* [FEATURE][EF-000] Commit for feature
* Merge branch feature/EF-000 into develop [this will not be logged]
* This commit will not be logged
* [EF-000] This commit will be logged

'''

# Returns the last tag commit hash, or the first commit hash if there are no tags
def get_last_commit():
    pipe = sp.Popen('git describe --abbrev=0 --tags', shell=True, stdout=sp.PIPE, stderr=sp.PIPE)
    prev_tag, err = pipe.communicate()

    if (pipe.returncode == 0):
        return prev_tag.strip()
    else:
        pipe = sp.Popen('git rev-list --max-parents=0 HEAD', shell=True, stdout=sp.PIPE, stderr=sp.PIPE)
        first_commit, err = pipe.communicate()

        if (pipe.returncode == 0):
            return first_commit.strip()
        else:
            print('Error: Could not get the last commit hash')
            print(err.strip())
            sys.exit(1)


def get_commits():
    # Get the previous tag
    prev_tag = get_last_commit().decode('utf-8')

    print('Previous tag: ' + prev_tag)

    # Get the commits since the previous tag
    pipe = sp.Popen('git rev-list ' + prev_tag + '..HEAD --format=%s', shell=True, stdout=sp.PIPE, stderr=sp.PIPE)

    commits, err = pipe.communicate()

    if (pipe.returncode == 0):
        commits = commits.strip().decode('utf-8').split('\n')
    else:
        print('$ git rev-list ' + prev_tag + '..HEAD')
        print('Error: Could not get the commits')
        print(err.strip())
        sys.exit(1)

    return commits

def get_formatted_messages(commits):
    messages = []

    print('Commits: ' + str(commits))

    for commit in commits:
        message = ""
        message_parts = commit.split('[')
        for m in message_parts:
            if m.startswith('EF-'):
                if m.startswith('EF-0'):
                    message = m.replace(']', ':')
                else:
                    message = m.split(']')[0]
                break

        if message.startswith('EF-'):
            if message.startswith('EF-0'):
                messages.append(message)
            else:
                # jira_summary = get_jira_summary(message)
                messages.append(message + ": <Jira summary>")

    print('Messages: ' + str(messages))

    return set(messages)

def get_jira_summary(jira_task):
    # Get the Jira summary from the Jira task
    jira_summary = os.popen('curl -s -u "username:password" -X GET -H "Content-Type: application/json" https://jira.domain.com/rest/api/2/issue/' + jira_task + ' | jq -r \'.fields.summary\'').read().strip()
    return jira_summary

def send_slack_message(messages, version):
    # Send a message to a Slack channel
    slack_message = 'Version ' + version + ' deployed\n'
    for message in messages:
        slack_message += message + '\n'
    # os.system('curl -X POST -H \'Content-type: application/json\' --data \'{"text":"' + slack_message + '"}\' https://hooks.slack.com/services/XXXXXXXXX/XXXXXXXXX/XXXXXXXXXXXXXXXXXXXXXXXX')
    os.system('echo "' + slack_message + '"')

    # Save into 'changelog.txt'
    with open('changelog.txt', 'w') as f:
        f.write(slack_message)

def main():
    # Get the version number
    version = sys.argv[1]
    # Get the commits
    commits = get_commits()
    # Get the Jira tasks from the commits
    messages = get_formatted_messages(commits)
    # Send a Slack message
    send_slack_message(messages, version)

if __name__ == '__main__':
    main()